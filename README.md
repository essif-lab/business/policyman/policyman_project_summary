# Our product
Our existing product is an application independent verifiable credentials middleware, 
that will plug into any application layer protocol via 3 simple APIs for integration 
into the VC Issuer, VC Holder and VC Verifier. 
The new product we will develop for eSSIF is a policy management tool that will allow 
resource owners e.g. web sites, hotels, etc. to easily specify which VCs they require 
from users in order for the users to access their resources. Different policies can 
be specified for each resource, so for example, each web page at a web site could 
have a different policy, or different rooms in a hotel could have different policies.

# Value proposition – what is/are the benefit(s) for your customer
The VP for our application independent verifiable credentials middleware is:

We provide the virtual equivalent of physical credentials (plastic cards, passports, 
qualifications, driving licenses etc.) for everyone and everything by converting 
them into cryptographically secure and privacy protecting W3C Verifiable Credentials 
so that they can be easily stored, carried and presented electronically as needed.

The VP for our policy management tool is:

We provide resource owners with an easy way of controlling access to their resources 
by providing them with a user-friendly graphical interface and supporting infrastructure 
so that they can easily specify and enforce their policies for which verifiable 
credentials users must have in order to access their resources.
